<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, ['label' => 'Login'])
            ->add('email', null, [
                'label' => 'Adres email',
                'constraints' => [
                    new Email([
                        'message' => 'Zła wartość',
                    ])
                ]
            ])
            ->add('first_name', null, ['label' => 'Imię'])
            ->add('last_name', null, ['label' => 'Nazwisko'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
