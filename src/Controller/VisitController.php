<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Visit;
use FOS\UserBundle\Mailer\Mailer;
use App\Utils\AppMailer;

/**
 * @Route("/visit")
 */
class VisitController extends AbstractController
{
  /**
   * @Route("/{id}/sign", name="visit_sign", methods={"POST"})
   */
  public function visitSign(Request $request, Visit $visit): Response
  {
    if ($this->isCsrfTokenValid('sign'.$visit->getId(), $request->request->get('_token'))) {
      $user = $this->getUser();
      $visit->signIn($user);
      $this->getDoctrine()->getManager()->flush();
      $this->addFlash('success', 'Wizyta pomyślnie umówiona.');
    }
    return $this->redirectToRoute('profile_visit_index');
  }

  /**
   * @Route("/{id}/signout", name="visit_signout", methods={"POST"})
   */
  public function visitSignout(Request $request, Visit $visit, \Swift_Mailer $mailer): Response
  {

    if ($visit->getUser()->getId() != $this->getUser()->getId()) {
      $this->addFlash('danger', 'Ta wizyta nie należy do zalogowanego użytkownika');
      return $this->redirectToRoute('profile_visit_index');
    }
    if ($this->isCsrfTokenValid('signout'.$visit->getId(), $request->request->get('_token'))) {
      $appMailer = new AppMailer($mailer);
      $appMailer->sendMail(
        'Wizyta odwołana',
        $this->renderView('emails/signout.html.twig')
      );
      $visit->signOut();
      $this->getDoctrine()->getManager()->flush();
      $this->addFlash('success', 'Wizyta pomyślnie odwołana.');
    }
    return $this->redirectToRoute('profile_visit_index');
  }
}
