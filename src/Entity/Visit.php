<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Annotations\Annotation\Enum;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VisitRepository")
 */
class Visit
{
    const STATUS_OPEN = 'open';
    const STATUS_RESERVED = 'reserved';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="visits")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Doctor", inversedBy="visits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /** 
     * @ORM\Column(type="string", columnDefinition="ENUM('open', 'reserved')")
     * 
    */
    private $status = 'open';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDoctor(): ?Doctor
    {
        return $this->doctor;
    }

    public function setDoctor(?Doctor $doctor): self
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, array(self::STATUS_OPEN, self::STATUS_RESERVED))) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->status = $status;

        return $this;
    }

    public function signIn(User $user): self {
        $this->setUser($user);
        $this->setStatus(self::STATUS_RESERVED);
        return $this;
    }

    public function signOut(): self {
        $this->setUser(null);
        $this->setStatus(self::STATUS_OPEN);
        return $this;
    }
}
