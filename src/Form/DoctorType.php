<?php

namespace App\Form;

use App\Entity\Doctor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class DoctorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', null, ['label' => 'Imię'])
            ->add('last_name', null, ['label' => 'Nazwisko'])
            ->add('email', null, [
                'label' => 'Adres email',
                'constraints' => [
                    new Email([
                        'message' => 'Zła wartość',
                    ])
                ]
            ])
            ->add('phone', null, ['label' => 'Telefon'])
            ->add('specialization', null, ['label' => 'Specjalizacja'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Doctor::class,
        ]);
    }
}
