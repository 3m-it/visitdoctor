<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Doctor;
use App\Entity\Visit;
use App\Form\VisitType;

/**
 * @Route("/admin/visit")
 */
class VisitController extends AbstractController
{

  /**
   * @Route("/{id}/edit", name="visit_edit", methods={"GET","POST"})
   */
  public function edit(Request $request, Visit $visit): Response
  {
      $doctor = $visit->getDoctor();
      $form = $this->createForm(VisitType::class, $visit);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $this->getDoctrine()->getManager()->flush();

          return $this->redirectToRoute('doctor_visit_index', [
              'id' => $doctor->getId(),
          ]);
      }

      return $this->render('admin/visit/edit.html.twig', [
          'visit' => $visit,
          'doctor' => $doctor,
          'form' => $form->createView(),
      ]);
  }
}
