import 'tempusdominus-bootstrap-4';

$(function() {
  // Datetime picker initialization.
  // See http://eonasdan.github.io/bootstrap-datetimepicker/
  $('.datetimepicker').datetimepicker({
    locale: 'pl',
    format: 'YYYY-MM-DDTHH:mm:ss'
  });

  if ($('#admin-user-index').length > 0) {
    $('[name="roleAdmin"]').on('change', function() {
        $(this).parents('form').submit();
    });
  }
});