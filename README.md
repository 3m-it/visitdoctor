
## tworzenie nowego projektu
composer create-project symfony/skeleton <project-name>

## MakerBundle
https://symfony.com/doc/current/bundles/SymfonyMakerBundle/index.html
#### lista funkcji
```php bin/console list make```
#### tworzy automatycznie klase User
```php bin/console make:user```
#### tworzy plik w rodzaju flyway. Zapisuje go w aplikacji.
```php bin/console make:migration```
##### Wykorzystuje plik z make:migration i poprzez docrtine wykonuje SQL do bazy
```php bin/console doctrine:migrations:migrate```
#### tworzy zasoby potrzebne do autoryzacji uzytkownikow
```php bin/console make:auth```
#### Komendy potrzebne do logowania/rejestracji użytkownika (po kolei)
```
make:user
// trzeba zwrócić uwagę na plik config/packages/doctrine.yaml -> doctrine.dbal - czy dane bazy są ok
php bin/console make:migration
php bin/console doctrine:migrations:migrate
make:auth
make:registration-form
```