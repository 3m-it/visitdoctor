<?php

namespace App\Utils;

class AppMailer
{
  /**
   * @var \Swift_Mailer
   */
  private $_mailer;
  
  public function __construct(\Swift_Mailer $mailer) {
    $this->_mailer = $mailer;
  }

  public function sendMail(string $title, string $body) {
    $message = (new \Swift_Message($title))
      ->setFrom('truedue@gmail.com')
      ->setTo('truedue@gmail.com')
      ->setBody($body, 'text/html')
    ;
    $this->_mailer->send($message);
  }
}
